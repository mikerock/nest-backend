import { Injectable } from '@nestjs/common';
import { Application } from 'express';
@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }
}

@Injectable()
export class ExpressService {
  constructor(private application: Application) {}
  get app() {
    return this.application;
  }
}
