import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService, ExpressService } from './app.service';
import { Application } from 'express';

@Module({
  imports: [],
  providers: [
    {
      provide: ExpressService,
      useValue: {
        get app() {
          let app;
          return (() => {
            return app ? app : (app = new ExpressService(ConfigModule.app));
          })();
        },
      },
    },
  ],
  exports: [ExpressService],
})
export class ConfigModule {
  static app: Application;
}

@Module({
  imports: [ConfigModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
