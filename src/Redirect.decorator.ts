import { ReflectMetadata } from '@nestjs/common';

export const Redirect = (to: string) => (
  target: any,
  propertyKey: string,
  descriptor: TypedPropertyDescriptor<any>,
): TypedPropertyDescriptor<any> => {
  return descriptor;
};
