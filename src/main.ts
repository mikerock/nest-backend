import { NestFactory } from '@nestjs/core';
import { Module } from '@nestjs/common';
import { AppModule, ConfigModule } from './app.module';
import * as express from 'express';
import * as dotenv from 'dotenv';

dotenv.config();

const PORT = process.env.PORT || 8000;
const instance = express();
// Necesary to pass the instance to ExpressService
ConfigModule.app = instance;
async function bootstrap() {
  const app = await NestFactory.create(AppModule, instance);
  await app.listen(PORT, () => {
    console.log(`LISTENING ON PORT ${app.getHttpServer().address().port}`);
  });
}
bootstrap();
