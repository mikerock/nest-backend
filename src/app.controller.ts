import { Controller, Get, Req, Res, Next, Param } from '@nestjs/common';
import { AppService, ExpressService } from './app.service';
import { Request, Response, NextFunction } from 'express';
import { Redirect } from './Redirect.decorator';
@Controller()
export class AppController {
  constructor(
    public readonly appService: AppService,
    private expressService: ExpressService,
  ) {
    console.log(this.expressService.app);
  }

  @Get()
  @Redirect('/')
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('/api/(:param)?')
  get(
    @Param('param') param: string,
    @Req() req: Request,
    @Res() res: Response,
    @Next() next: NextFunction,
  ) {
    res.send({ message: `Sent param: ${param}` });
  }
}
